const myHeading = document.querySelector('h1');
myHeading.textContent = 'Hello world!';

console.log('The value of b is ' + b); // The value of b is undefined
var b;

//conditionals
let iceCream = 'chocolate';
if(iceCream === 'chocolate') {
  alert('Yay, I love chocolate ice cream!');    
} else {
  alert('Awwww, but chocolate is my favorite...');    
}


//functions

function multiply(num1,num2) {
    let result = num1 * num2;
    return result;
  }

console.log(multiply(4, 7));
console.log(multiply(20, 20));
document.write(multiply(0.5, 3));